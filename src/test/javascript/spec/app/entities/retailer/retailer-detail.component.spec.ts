import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { RetailerDetailComponent } from '../../../../../../main/webapp/app/entities/retailer/retailer-detail.component';
import { RetailerService } from '../../../../../../main/webapp/app/entities/retailer/retailer.service';
import { Retailer } from '../../../../../../main/webapp/app/entities/retailer/retailer.model';

describe('Component Tests', () => {

    describe('Retailer Management Detail Component', () => {
        let comp: RetailerDetailComponent;
        let fixture: ComponentFixture<RetailerDetailComponent>;
        let service: RetailerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [RetailerDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    RetailerService
                ]
            }).overrideComponent(RetailerDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RetailerDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RetailerService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Retailer(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.retailer).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
