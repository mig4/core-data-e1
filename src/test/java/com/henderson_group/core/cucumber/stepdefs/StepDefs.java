package com.henderson_group.core.cucumber.stepdefs;

import com.henderson_group.core.CoreDataE1App;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = CoreDataE1App.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
