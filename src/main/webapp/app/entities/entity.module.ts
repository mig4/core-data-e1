import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CoreDataE1AddressModule } from './address/address.module';
import { CoreDataE1CommodityModule } from './commodity/commodity.module';
import { CoreDataE1RetailerModule } from './retailer/retailer.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        CoreDataE1AddressModule,
        CoreDataE1CommodityModule,
        CoreDataE1RetailerModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreDataE1EntityModule {}
