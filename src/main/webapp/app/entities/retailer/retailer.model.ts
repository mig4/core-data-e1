import { Address } from '../address';
export class Retailer {
    constructor(
        public id?: number,
        public code?: number,
        public name?: string,
        public address?: Address,
    ) {
    }
}
