import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreDataE1SharedModule } from '../../shared';

import {
    RetailerService,
    RetailerPopupService,
    RetailerComponent,
    RetailerDetailComponent,
    RetailerDialogComponent,
    RetailerPopupComponent,
    RetailerDeletePopupComponent,
    RetailerDeleteDialogComponent,
    retailerRoute,
    retailerPopupRoute,
} from './';

let ENTITY_STATES = [
    ...retailerRoute,
    ...retailerPopupRoute,
];

@NgModule({
    imports: [
        CoreDataE1SharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RetailerComponent,
        RetailerDetailComponent,
        RetailerDialogComponent,
        RetailerDeleteDialogComponent,
        RetailerPopupComponent,
        RetailerDeletePopupComponent,
    ],
    entryComponents: [
        RetailerComponent,
        RetailerDialogComponent,
        RetailerPopupComponent,
        RetailerDeleteDialogComponent,
        RetailerDeletePopupComponent,
    ],
    providers: [
        RetailerService,
        RetailerPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreDataE1RetailerModule {}
