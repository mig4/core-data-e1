import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { Retailer } from './retailer.model';
import { RetailerPopupService } from './retailer-popup.service';
import { RetailerService } from './retailer.service';

@Component({
    selector: 'jhi-retailer-delete-dialog',
    templateUrl: './retailer-delete-dialog.component.html'
})
export class RetailerDeleteDialogComponent {

    retailer: Retailer;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private retailerService: RetailerService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['retailer']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.retailerService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'retailerListModification',
                content: 'Deleted an retailer'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-retailer-delete-popup',
    template: ''
})
export class RetailerDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private retailerPopupService: RetailerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.retailerPopupService
                .open(RetailerDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
