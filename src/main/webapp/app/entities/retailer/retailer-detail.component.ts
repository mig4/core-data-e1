import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { Retailer } from './retailer.model';
import { RetailerService } from './retailer.service';

@Component({
    selector: 'jhi-retailer-detail',
    templateUrl: './retailer-detail.component.html'
})
export class RetailerDetailComponent implements OnInit, OnDestroy {

    retailer: Retailer;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private retailerService: RetailerService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['retailer']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.retailerService.find(id).subscribe(retailer => {
            this.retailer = retailer;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
