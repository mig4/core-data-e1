import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { RetailerComponent } from './retailer.component';
import { RetailerDetailComponent } from './retailer-detail.component';
import { RetailerPopupComponent } from './retailer-dialog.component';
import { RetailerDeletePopupComponent } from './retailer-delete-dialog.component';

import { Principal } from '../../shared';


export const retailerRoute: Routes = [
  {
    path: 'retailer',
    component: RetailerComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.retailer.home.title'
    }
  }, {
    path: 'retailer/:id',
    component: RetailerDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.retailer.home.title'
    }
  }
];

export const retailerPopupRoute: Routes = [
  {
    path: 'retailer-new',
    component: RetailerPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.retailer.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'retailer/:id/edit',
    component: RetailerPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.retailer.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'retailer/:id/delete',
    component: RetailerDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.retailer.home.title'
    },
    outlet: 'popup'
  }
];
