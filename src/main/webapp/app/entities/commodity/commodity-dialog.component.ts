import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { Commodity } from './commodity.model';
import { CommodityPopupService } from './commodity-popup.service';
import { CommodityService } from './commodity.service';

@Component({
    selector: 'jhi-commodity-dialog',
    templateUrl: './commodity-dialog.component.html'
})
export class CommodityDialogComponent implements OnInit {

    commodity: Commodity;
    authorities: any[];
    isSaving: boolean;
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private commodityService: CommodityService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['commodity', 'commodityStatus']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.commodity.id !== undefined) {
            this.commodityService.update(this.commodity)
                .subscribe((res: Commodity) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.commodityService.create(this.commodity)
                .subscribe((res: Commodity) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: Commodity) {
        this.eventManager.broadcast({ name: 'commodityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-commodity-popup',
    template: ''
})
export class CommodityPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private commodityPopupService: CommodityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.commodityPopupService
                    .open(CommodityDialogComponent, params['id']);
            } else {
                this.modalRef = this.commodityPopupService
                    .open(CommodityDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
