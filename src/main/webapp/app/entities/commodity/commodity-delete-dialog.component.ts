import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, JhiLanguageService } from 'ng-jhipster';

import { Commodity } from './commodity.model';
import { CommodityPopupService } from './commodity-popup.service';
import { CommodityService } from './commodity.service';

@Component({
    selector: 'jhi-commodity-delete-dialog',
    templateUrl: './commodity-delete-dialog.component.html'
})
export class CommodityDeleteDialogComponent {

    commodity: Commodity;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private commodityService: CommodityService,
        public activeModal: NgbActiveModal,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['commodity', 'commodityStatus']);
    }

    clear () {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete (id: number) {
        this.commodityService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'commodityListModification',
                content: 'Deleted an commodity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-commodity-delete-popup',
    template: ''
})
export class CommodityDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private commodityPopupService: CommodityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            this.modalRef = this.commodityPopupService
                .open(CommodityDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
