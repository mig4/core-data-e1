import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { Commodity } from './commodity.model';
import { CommodityService } from './commodity.service';

@Component({
    selector: 'jhi-commodity-detail',
    templateUrl: './commodity-detail.component.html'
})
export class CommodityDetailComponent implements OnInit, OnDestroy {

    commodity: Commodity;
    private subscription: any;

    constructor(
        private jhiLanguageService: JhiLanguageService,
        private commodityService: CommodityService,
        private route: ActivatedRoute
    ) {
        this.jhiLanguageService.setLocations(['commodity', 'commodityStatus']);
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe(params => {
            this.load(params['id']);
        });
    }

    load (id) {
        this.commodityService.find(id).subscribe(commodity => {
            this.commodity = commodity;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
