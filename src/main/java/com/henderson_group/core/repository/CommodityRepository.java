package com.henderson_group.core.repository;

import com.henderson_group.core.domain.Commodity;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Commodity entity.
 */
@SuppressWarnings("unused")
public interface CommodityRepository extends JpaRepository<Commodity,Long> {

}
