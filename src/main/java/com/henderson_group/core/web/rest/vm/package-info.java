/**
 * View Models used by Spring MVC REST controllers.
 */
package com.henderson_group.core.web.rest.vm;
